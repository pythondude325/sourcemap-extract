use anyhow::{anyhow, Context};
use serde::Deserialize;
use std::fs;
use std::path::PathBuf;
use structopt::StructOpt;

#[derive(Deserialize)]
struct SourceMap {
    sources: Vec<String>,
    #[serde(rename = "sourcesContent")]
    sources_content: Vec<String>,
}

#[derive(StructOpt)]
struct Opt {
    /// The path to the source map file
    source_map_path: PathBuf,

    /// The destination for the extracted files. NOTE: Extracted files are
    /// allowed to use relative paths, so files may be created above the
    /// specified directory.
    destination_directory: PathBuf,
}

fn main() -> anyhow::Result<()> {
    let opt = Opt::from_args();

    let source_map_string =
        fs::read_to_string(opt.source_map_path).context("Failed to read source map")?;
    let source_map: SourceMap =
        serde_json::from_str(&source_map_string).context("Failed to parse source map JSON")?;

    if source_map.sources.len() != source_map.sources_content.len() {
        return Err(anyhow!(
            "Source map file is invalid (sources does not match sourcesContent"
        ));
    }

    println!("Extracting source files...");

    let progress_bar = indicatif::ProgressBar::new(source_map.sources.len() as u64);
    for (source_path, source_content) in source_map
        .sources
        .iter()
        .zip(source_map.sources_content.iter())
    {
        let destination_path = opt.destination_directory.join(source_path);

        fs::create_dir_all(destination_path.parent().unwrap())
            .with_context(|| format!("Failed to create directory for {}", source_path))?;
        fs::write(destination_path, source_content)
            .with_context(|| format!("Failed to write source for {}", source_path))?;
        progress_bar.inc(1);
    }

    progress_bar.finish();

    println!("Finished");

    Ok(())
}
